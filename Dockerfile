# Ngrok Server
#
# VERSION               0.0.1
#

FROM  daocloud.io/library/ubuntu:16.04
#FROM  hub.c.163.com/public/ubuntu:16.04
MAINTAINER Andy Fox <andy.fox@qq.com>
LABEL com.ngrokd.version="0.0.1"

ADD ngrokd.tar.gz /
ADD svr.sh /svr.sh
ADD run.sh /run.sh

RUN chmod +x /ngrokd
RUN chmod +x /svr.sh 
RUN chmod +x /run.sh


EXPOSE 24443
EXPOSE 31415


ENTRYPOINT ["/svr.sh"]
