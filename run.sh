#!/bin/bash

count=`ps -ef | grep ngrokd | grep -v "grep" | wc -l`

while :
do
  echo $count
  if [ $count -gt 0 ]; then
    sleep 30
  else
    echo "Restart ngrokd..."
    /usr/bin/ngrok/svr.sh&
  fi
done
